---
home: true
heroImage: /hero.png
actionText: Get Started →
---

# Hokodo Design System

This style guide is based largely on the principles of atomic design. The key idea of this methodology is that small, independent - atomic - parts, can be combined into larger molecular structures. Molecular structures can be combined into larger organisms, which can then serve as the foundation for templates and full pages.