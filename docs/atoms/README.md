# Introduction

Atoms are the smallest basic structures of an user interface. They cannot be broken down any further. These atoms include basic HTML elements like form labels, inputs, buttons, and others that can’t be broken down any further without ceasing to be functional.

Atoms utilize decisions made on the design token level.

interface atoms don’t exist in a vacuum and only really come to life with application.
