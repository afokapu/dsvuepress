# Avatars

Avatars display an image if the user has uploaded one but if not, initials are shown instead.

The background color is chosen randomly for each user and set inline on `.avatar`. The image URL is set inline on `.avatar-img`. Additionally, each avatar should have a `title` attribute with the user's name.