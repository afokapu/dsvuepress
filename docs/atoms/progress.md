# Progress

## Progress bar

A progress bar component communicates to the user the progress of a particular process.

## Progress indicator

A progress indicator communicates to the user the state of a step in a particular process.