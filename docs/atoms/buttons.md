# Buttons

## Button sizes

Define a button size with ̀`.button--s`, `.button--m` or ̀`.button--l`. The default button size is ̀`.button--m`.

## Ghost button

Ghost buttons should be used to signify optional, infrequent or subtle actions.

## Disabled button

Disabled buttons receive a `.disabled` class attribute.

## Icon button