# Introduction

Pages are specific instances of templates that show what a UI looks like with real representative content in place. This is what users will see and interact with

In addition to demonstrating the final interface as your users will see it, pages are essential for testing the effectiveness of the underlying design system. It is at the page stage that we’re able to take a look at how all those patterns hold up when real content is applied to the design system. Does everything look great and function as it should? If the answer is no, then we can loop back and modify our molecules, organisms, and templates to better address our content’s needs.