# Introduction

An organism is a complex component that’s made up of atoms, molecules, or other organisms. A grid of cards is a common pattern you see on a whole bunch of sites.

How to differenciate a molecule from an organism? We use two parameter: size, hierachy and complexity.

An organism takes more than half of the view port (with and/or height), it offers a decent number of features, and contains a fair bit of content.

Ex: a lookup bar is molecule but a header contining a lookup and navigation menu, logo and avatar is an organism.