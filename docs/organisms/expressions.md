# Expression

Expression builders help users declaratively construct logical expressions. These expressions can be used when querying for a filtered set of records, creating rules to control when something executes, or any other conditional logic.