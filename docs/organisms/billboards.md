# Billboard

Billboards are defined by use of a full-width background, a headline and supporting copy, with equal padding above and below the content. Where necessary we can use a background colour instead of an image. Billboards are particularly useful for landing pages or onboarding where we want to introduce campaign or marketing messaging.

The content of a billboard is somewhat flexible, though it should include:

- a background image or colour

- a heading (h1)
supporting text (uno)

Optionally a billboard can include:

- a logo or an icon

- a call to action below the supporting copy

The call to action should contain a maximum of two buttons.

