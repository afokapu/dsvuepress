# Colors

## Brand colors

Primary brand colors are used for elements that must reflect Hokodo's brand. Each color has a darker and a lighter shade.

## Background colors

Use these tokens for background colors only. Do not use these for border colors or text color

## Text colors

Use these tokens for text colors only. Do not use these for border colors or background colors.

## Border colors

Use these tokens for border colors only. Do not use these for text colors or background colors.