# Typography

## Font family

We almost exclusively use Titillium Web as the base typeface across the entire Hokodo product. Maintaining typographic clarity and hierarchy is important; here is a showcase of different ways to structure content:

## Font sizes

The font scale consists of 9 different font sizes. The default size applied to the `body` is Medium

## Font weight

There are three font weights available to use — Blond, Regular and Medium. These can be applied with `.fontWeight-3`, `.fontWeight-4` and `.fontWeight-5` respectively.

## Line height

## Alignement classes

Left, right or center align text with text alignment classes.

## Transformation classes

Transform text with text transformation classes.
