# Forms

Most commonly you want the form to handle a set of data and display appropriate input fields for each piece of data.

A form element is made up of three primary elements, the label, a container and the input.
