# Cards

Cards are used to apply a container around a related grouping of information.

A card is made up of 3 sections, a header, a body, and a footer. The header and footer have limitations, but the body section can accommodate any layout of related information.