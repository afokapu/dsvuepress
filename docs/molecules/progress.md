# Run progress

Run progress allows learners to navigate through the content of a specific course run. It is displayed in a form of interactive progress bar.