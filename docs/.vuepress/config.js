module.exports = {
    title: 'Hokodo Design System',
    description: 'HDS is Hokodo style guide',
    base: '/dsvuepress/',
    dest: 'public',
    themeConfig: {
        // nav header
        nav: [
            { text: 'Atoms', link: '/atoms/' },
            { text: 'hOKODO', link: 'https://hokodo.co/' }
        ],
        
        // search build in
        //search: true,
        //searchMaxSuggestions: 10,
        
        //sidebar: 'auto'
        sidebar: [
            {
                title: 'Design Token',
                collapsable: true,
                children: [
                    '/tokens/',
                    '/tokens/typography',
                    '/tokens/colors',
                    '/tokens/borders',
                    '/tokens/spacing',
                    '/tokens/opacity',
                    '/tokens/size',
                    '/tokens/layout',
                    '/tokens/animations',
                    '/tokens/media'
                ]
            },

            {
                title: 'Atom',
                collapsable: true,
                children: [
                    '/atoms/',
                    '/atoms/avatars',
                    '/atoms/buttons',
                    '/atoms/icons',
                    '/atoms/illustrations',
                    '/atoms/inputs',
                    '/atoms/progress',
                    '/atoms/toasts',
                    '/atoms/tooltips'
                ]
            },

            {
                title: 'Molecules',
                collapsable: true,
                children:[
                    '/molecules/',
                    '/molecules/cards',
                    '/molecules/datetime',
                    '/molecules/dropzone',
                    '/molecules/lookup',
                    '/molecules/progress',
                    '/molecules/tiles'
                ]
            },

            {
                title: 'Organisms',
                collapsable: true,
                children:[
                    '/organisms/',
                    '/organisms/billboards',
                    '/organisms/datatables',
                    '/organisms/expressions',
                    '/organisms/footers',
                    '/organisms/headers',
                    '/organisms/sidebars'
                ]
            },

            {
                title: 'Templates',
                collapsable: true,
                children:[
                    '/templates/',
                    '/templates/settings',
                    '/templates/fullpageforms',
                    '/organisms/account'
                ]
            },            {
                title: 'Pages',
                collapsable: true,
                children:[
                    '/pages/',
                    '/pages/onboarding',
                    '/pages/profile'
                ]
            } 
        ]
    }
}